package pl.java;

public class OldCar extends Car {

	private final static int DIV_VALUE = 2;
	
	public void speedUp(double speedIncrease) {
		this.speed += divideSpeed(speedIncrease);
	}
	
	public void speedDown(double speedDecrease) {
		this.speed -= divideSpeed(speedDecrease);
	}
	
	private double divideSpeed(double speedToDivide) {
		return speedToDivide / DIV_VALUE;
	}
	
}
