package pl.java;

public class Car {
	
	protected double speed = 0;
	private int gear = 1;
	
	public void gearUp() {
		this.gear++;
	}
	
	public void gearDown() {
		this.gear--;
	}
	
	public void speedUp(double speedIncrease) {
		this.speed += speedIncrease;
	}
	
	public void speedDown(double speedDecrease) {
		this.speed -= speedDecrease;
	}
	
	public double getSpeed() {
		return speed;
	}

	public int getGear() {
		return gear;
	}
}
